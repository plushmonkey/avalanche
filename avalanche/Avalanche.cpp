#include "Avalanche.h"
#include "Factory.h"
#include "login/LoginFlood.h"
#include <mclib/util/VersionFetcher.h>
#include <mclib/common/Json.h>

#include <fstream>
#include <memory>
#include <iostream>

namespace avalanche {

void Avalanche::Run() {
    if (m_Instances.empty()) {
        return;
    }

    s32 activeInstances = m_LoginMethod->Login(m_Instances);

    std::cout << "Successfully logged in " << activeInstances << "/" << m_Instances.size() << " instances." << std::endl;

    while (activeInstances > 0) {
        for (auto&& instance : m_Instances) {
            if (!instance->Update()) {
                --activeInstances;
                std::cout << "Instance was kicked from server. " << activeInstances << " instances remaining." << std::endl;
            }
        }
    }
}

bool Avalanche::Initialize(const OptionMap& options) {
    auto jsonIter = options.find("json");
    mc::json behaviorNode;
    std::string behaviorMethod;
    std::size_t count = 1;

    auto behaviorIter = options.find("behavior");
    if (behaviorIter != options.end()) {
        behaviorMethod = behaviorIter->second;
    }

    if (jsonIter != options.end()) {
        std::string jsonFile = jsonIter->second;
        mc::json root;
        std::ifstream in(jsonFile);

        if (!in.is_open()) {
            throw std::runtime_error("Failed to read JSON file " + jsonFile);
        }

        try {
            root = mc::json::parse(in);
        } catch (mc::json::exception& e) {
            std::cerr << e.what() << std::endl;
            throw std::runtime_error("Failed to parse JSON file " + jsonFile);
        }

        auto& countNode = root.value("count", mc::json());

        if (countNode.is_number_integer()) {
            count = countNode.get<int>();
        }

        auto& loginNode = root.value("login", mc::json());
        if (loginNode.is_object()) {
            auto& methodNode = loginNode.value("method", mc::json());
            std::string methodName = LoginFlood::s_Name;

            if (methodNode.is_object()) {
                auto& methodNameNode = methodNode.value("name", mc::json());
                if (methodNameNode.is_string()) {
                    methodName = methodNameNode.get<std::string>();
                }
            }

            m_LoginMethod = g_LoginFactory.Create(methodName);

            if (m_LoginMethod) {
                if (!m_LoginMethod->ReadJSON(loginNode)) {
                    std::cerr << "Failed to read login method JSON." << std::endl;
                    return false;
                }
            }
        }

        if (behaviorMethod.empty()) {
            behaviorNode = root.value("behavior", mc::json());
            if (behaviorNode.is_object()) {
                auto& methodNode = behaviorNode.value("method", mc::json());
                if (methodNode.is_string()) {
                    behaviorMethod = methodNode.get<std::string>();
                }
            }
        }
    }

    if (m_LoginMethod == nullptr) {
        m_LoginMethod = g_LoginFactory.Create(LoginFlood::s_Name);
    }

    if (!m_LoginMethod->ReadOptions(options)) {
        return false;
    }

    auto countIter = options.find("count");
    if (countIter != options.end()) {
        count = strtol(countIter->second.c_str(), nullptr, 10);
    }

    mc::util::VersionFetcher versionFetcher(m_LoginMethod->GetHost(), m_LoginMethod->GetPort());
    auto version = versionFetcher.GetVersion();

    mc::block::BlockRegistry::GetInstance()->RegisterVanillaBlocks(version);

    m_Instances.reserve(count);

    for (std::size_t i = 0; i < count; ++i) {
        m_Instances.emplace_back(std::make_unique<Instance>(version));
    }

    if (g_BehaviorFactory.Contains(behaviorMethod)) {
        std::cout << "behavior: " << behaviorMethod << std::endl;
    } else {
        std::cout << "behavior: none" << std::endl;
    }

    for (auto&& instance : m_Instances) {
        if (!behaviorMethod.empty()) {
            std::unique_ptr<Behavior> behavior = g_BehaviorFactory.Create(behaviorMethod, instance->GetClient(), version);

            if (behavior && !behaviorNode.is_null()) {
                try {
                    if (!behavior->ReadJSON(behaviorNode)) {
                        std::cerr << "Error reading behavior JSON" << std::endl;

                        m_Instances.clear();
                        return true;
                    }
                } catch (const std::exception& e) {
                    std::cerr << "Behavior error: " << e.what() << std::endl;

                    m_Instances.clear();
                    return true;
                }
            }

            if (behavior) {
                behavior->OnCreate();
            }

            instance->SetBehavior(std::move(behavior));
        }
    }

    return true;
}

} // ns avalanche
