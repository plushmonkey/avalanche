#include "BehaviorMessage.h"

#include <mclib/common/Json.h>
#include <mclib/util/Utility.h>

namespace avalanche {

const char* BehaviorMessage::s_Name = "message";

BehaviorMessage::BehaviorMessage(mc::core::Client* client, mc::protocol::Version version)
    : m_Client(client)
{

}

void BehaviorMessage::OnCreate() {
    m_Finished = false;
    m_Client->RegisterListener(this);
}

bool BehaviorMessage::OnUpdate() {
    return m_Finished;
}

void BehaviorMessage::OnDestroy() {
    m_Client->UnregisterListener(this);
}

void BehaviorMessage::OnTick() {
    if (m_Finished) return;
    if (!InLoadedChunk(*m_Client)) return;

    mc::protocol::packets::out::ChatPacket packet(m_Message);
    m_Client->GetConnection()->SendPacket(&packet);

    m_Finished = true;
}

bool BehaviorMessage::ReadJSON(const mc::json& node) {
    m_Message = node.value("message", "");

    return !m_Message.empty();
}

} // ns avalanche
