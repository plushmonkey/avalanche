#include "BehaviorView.h"

#include <mclib/common/Json.h>
#include <mclib/util/Utility.h>
#include <limits>

namespace avalanche {

const char* BehaviorView::s_Name = "view";

BehaviorView::BehaviorView(mc::core::Client* client, mc::protocol::Version version)
    : m_Client(client),
    m_Yaw(0),
    m_Pitch(0),
    m_YawSet(0),
    m_PitchSet(0),
    m_Finished(false)
{

}

void BehaviorView::OnCreate() {
    m_Finished = false;
    m_Client->RegisterListener(this);
}

bool BehaviorView::OnUpdate() {
    return m_Finished;
}

void BehaviorView::OnDestroy() {
    m_Client->UnregisterListener(this);
}

void BehaviorView::OnTick() {
    if (!InLoadedChunk(*m_Client)) return;

    m_Client->GetPlayerController()->SetYaw(m_Yaw);
    m_Client->GetPlayerController()->SetPitch(m_Pitch);

    m_Finished = true;
}

bool BehaviorView::ReadJSON(const mc::json& node) {
    auto& yawNode = node.value("yaw", mc::json());
    auto& pitchNode = node.value("pitch", mc::json());

    if (yawNode.is_number()) {
        m_Yaw = yawNode.get<float>() * (3.14159f / 180.0f);
        m_YawSet = true;
    }

    if (pitchNode.is_number()) {
        m_Pitch = pitchNode.get<float>() * (3.14159f / 180.0f);
        m_PitchSet = true;
    }

    return true;
}

} // ns avalanche
