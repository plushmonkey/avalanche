#include "BehaviorDelay.h"

#include <mclib/common/Json.h>
#include <mclib/util/Utility.h>

namespace avalanche {

const char* BehaviorDelay::s_Name = "delay";

BehaviorDelay::BehaviorDelay(mc::core::Client* client, mc::protocol::Version version)
    : m_Client(client),
      m_Delay(0)
{

}

void BehaviorDelay::OnCreate() {
    m_StartTime = mc::util::GetTime();
}

bool BehaviorDelay::OnUpdate() {
    return mc::util::GetTime() >= m_StartTime + m_Delay;
}

void BehaviorDelay::OnDestroy() {
    
}

bool BehaviorDelay::ReadJSON(const mc::json& node) {
    m_Delay = node.value("delay", 0);

    return m_Delay > 0;
}

} // ns avalanche
