#include "BehaviorSneak.h"

#include <mclib/common/Json.h>
#include <mclib/util/Utility.h>
#include <limits>

namespace avalanche {

const char* BehaviorSneak::s_Name = "sneak";

BehaviorSneak::BehaviorSneak(mc::core::Client* client, mc::protocol::Version version)
    : m_Client(client),
      m_Start(true),
      m_Stop(false),
      m_Finished(false)
{

}

void BehaviorSneak::OnCreate() {
    m_Finished = false;
    m_Client->RegisterListener(this);
}

bool BehaviorSneak::OnUpdate() {
    return m_Finished;
}

void BehaviorSneak::OnDestroy() {
    m_Client->UnregisterListener(this);
}

void BehaviorSneak::OnTick() {
    if (!InLoadedChunk(*m_Client)) return;

    using namespace mc::protocol::packets;

    auto eid = m_Client->GetEntityManager()->GetPlayerEntity()->GetEntityId();

    if (m_Start) {
        out::EntityActionPacket packet(eid, out::EntityActionPacket::Action::StartSneak);
        m_Client->GetConnection()->SendPacket(&packet);
    }

    if (m_Stop) {
        out::EntityActionPacket packet(eid, out::EntityActionPacket::Action::StopSneak);
        m_Client->GetConnection()->SendPacket(&packet);
    }

    m_Finished = true;
}

bool BehaviorSneak::ReadJSON(const mc::json& node) {
    m_Start = node.value("start", true);
    m_Stop = node.value("stop", false);

    return true;
}

} // ns avalanche
