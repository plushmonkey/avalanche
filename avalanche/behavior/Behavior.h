#ifndef _AVALANCHE_BEHAVIOR_H_
#define _AVALANCHE_BEHAVIOR_H_

#include "../Factory.h"

#include <mclib/common/JsonFwd.h>
#include <mclib/protocol/ProtocolState.h>

namespace mc {
namespace core {

class Client;

} // ns core
} // ns mc

namespace avalanche {

class Behavior {
public:
    virtual ~Behavior() { }

    virtual void OnCreate() { }
    // Returns true when ready to be stopped
    virtual bool OnUpdate() { return false; }
    virtual void OnDestroy() { }

    virtual bool ReadJSON(const mc::json& node) = 0;
};

using BehaviorFactory = Factory<Behavior, mc::core::Client*, mc::protocol::Version>;
extern BehaviorFactory g_BehaviorFactory;

bool InLoadedChunk(mc::core::Client& client);

} // ns avalanche

#endif
