#ifndef _AVALANCHE_BEHAVIOR_VIEW_H_
#define _AVALANCHE_BEHAVIOR_VIEW_H_

#include "Behavior.h"

#include <vector>
#include <memory>
#include <mclib/core/Client.h>
#include <mclib/common/JsonFwd.h>

namespace avalanche {

class BehaviorView : public Behavior, mc::core::ClientListener {
private:
    mc::core::Client* m_Client;
    float m_Yaw;
    float m_Pitch;
    bool m_YawSet;
    bool m_PitchSet;
    bool m_Finished;

public:
    BehaviorView(mc::core::Client* client, mc::protocol::Version version);

    void OnCreate() override;
    bool OnUpdate() override;
    void OnDestroy() override;

    void OnTick() override;

    bool ReadJSON(const mc::json& node) override;

    static const char* s_Name;
};


} // ns avalanche

#endif
