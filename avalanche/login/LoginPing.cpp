#include "LoginPing.h"

#include "../Instance.h"
#include "Authenticator.h"
#include "generator/IncrementalGenerator.h"
#include <iostream>
#include <mclib/common/Json.h>
#include <mclib/util/Utility.h>

namespace avalanche {

class PingDisconnector : public mc::core::ConnectionListener {
private:
    mc::core::Connection& m_Connection;

public:
    PingDisconnector(mc::core::Connection& connection) : m_Connection(connection) {
        m_Connection.RegisterListener(this);
    }

    ~PingDisconnector() {
        m_Connection.UnregisterListener(this);
    }

    void OnPingResponse(const mc::json& node) override {
        m_Connection.Disconnect();
    }
};

const char* LoginPing::s_Name = "ping";

LoginPing::LoginPing()
{

}

std::size_t LoginPing::Login(std::vector<std::unique_ptr<Instance>>& instances) {
    std::size_t total = 0;

    std::cout << "Pinging " << instances.size() << " times." << std::endl;

    std::vector<std::unique_ptr<PingDisconnector>> disconnectors;

    for (std::size_t i = 0; i < instances.size(); ++i) {
        if (i != 0) {
            auto time = mc::util::GetTime();
            // Update previous instances while waiting to log in the next one.
            for (std::size_t j = 0; j < i; ++j) {
                auto connState = instances[j]->GetClient()->GetConnection()->GetSocketState();

                if (connState == mc::network::Socket::Status::Connected) {
                    instances[j]->GetClient()->Update();
                }
            }
        }

        auto instance = instances[i].get();

        disconnectors.emplace_back(std::make_unique<PingDisconnector>(*instance->GetClient()->GetConnection()));
        instance->GetClient()->Ping(m_Host, m_Port, mc::core::UpdateMethod::Manual);

        ++total;
    }

    return total;
}

bool LoginPing::ReadMethodJSON(const mc::json& node) {
    return true;
}

} // ns avalanche
