#ifndef AVALANCHE_LOGIN_PING_H_
#define AVALANCHE_LOGIN_PING_H_

#include "LoginMethod.h"

namespace avalanche {

class LoginPing : public LoginMethod {
private:
    bool ReadMethodJSON(const mc::json& node) override;

public:
    LoginPing();

    std::size_t Login(std::vector<std::unique_ptr<Instance>>& instances);

    static const char* s_Name;
};

} // ns avalanche

#endif
